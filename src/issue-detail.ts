import { Requests } from "./requests";

let token: string, options: any;
chrome.storage.sync.get(["token", "options"], result => {
  token = result.token || "";
  options = result.options;
});
const issueId = document.location.pathname.split("/issues/")[1];
const projectPath = (<HTMLAnchorElement>document
  .querySelector(".context-header a")).href
  .split('gitlab.com/')[1];
const sidebarLabels = document.querySelector(".issuable-show-labels");

const removeLabel = async (e: MouseEvent) => {
  e.preventDefault();
  e.stopPropagation();
  const target = <HTMLSpanElement>e.target;

  const container = target.closest(".gl-label")
  target.remove();
  const labelToRemove = [...container.querySelectorAll('.gl-label-text')].map(part => part.textContent).join('::')

  try {
    const req = new Requests(token, projectPath);

    const issue = await req.getIssue(issueId);

    if (!issue.labels.includes(labelToRemove)) {
      alert("Label wasn't found on issue. It was probably removed by someone else in the meantime.");
    }
    const remainingLabels = issue.labels.filter(label => {
      return label !== labelToRemove;
    });

    req.updateLabels(issueId, remainingLabels);
    container.remove();
  } catch (error) {
    alert(error);
  }
};

if (sidebarLabels) {
  const labels = sidebarLabels.querySelectorAll(".gl-label-link");
  for (let label of labels) {
    if(label.parentElement.classList.contains('gl-label-scoped')) {
      label.children[1].insertAdjacentHTML(
        "beforeend",
        '<span class="gl-remove-label">X</span>'
      );
    }
    else{
      label.children[0].insertAdjacentHTML(
        "beforeend",
        '<span class="gl-remove-label"> X</span>'
      );
    }
  }
  const buttons = sidebarLabels.querySelectorAll(".gl-remove-label");
  for (let button of buttons) {
    button.addEventListener("click", removeLabel);
  }
}
