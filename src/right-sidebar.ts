import { Requests } from "./requests";

let token: string, options: any;
chrome.storage.sync.get(["token", "options"], result => {
  token = result.token || "";
  options = result.options;
});

const board = document.getElementById("content-body");

if (board) {
  // When mutation is observed
  const callback = function(mutationsList: MutationRecord[]) {
    //console.log(mutationsList);
    for (var mutation of mutationsList) {
      if (mutation.addedNodes.length) {
        for (let node of mutation.addedNodes) {
          const parentNode = <HTMLElement>node.parentNode;
          if (
            parentNode.classList.contains("issuable-show-labels") &&
            !parentNode.classList.contains("gl-tweaked")
          ) {
            if (!options.removeLabel) return;

            const labels = (<HTMLElement>node).querySelectorAll(".gl-label-link");

            for (let label of labels) {
              if(label.parentElement.classList.contains('gl-label-scoped')) {
                label.children[1].insertAdjacentHTML(
                  "beforeend",
                  '<span class="gl-remove-label">X</span>'
                );
              }
              else{
                label.children[0].insertAdjacentHTML(
                  "beforeend",
                  '<span class="gl-remove-label"> X</span>'
                );
              }
            }

            const removeLabel = async (e: MouseEvent) => {
              e.preventDefault();
              const target = <HTMLSpanElement>e.target;
              const issueId = (<HTMLSpanElement>(
                target
                  .closest(".issuable-sidebar")
                  .querySelector(".issuable-header-text span")
              )).innerText
                .trim()
                .substring(1);

              const button: HTMLButtonElement = target
                .closest(".block.labels")
                .querySelector(".js-label-select");

              const labelToRemove = target
                .closest(".gl-label")
                .classList.contains("gl-label-scoped") ? target
                    .closest(".gl-link")
                    .textContent.slice(0, -1)
                    .trim()
                    .replace(/\s+/g, "::") : target.parentElement.childNodes[0].nodeValue.trim();

              try {
                const req = new Requests(token, button.dataset.projectId);

                const issue = await req.getIssue(issueId);

                if (!issue.labels.includes(labelToRemove)) {
                  alert("Label wasn't found on issue. It was probably removed by someone else in the meantime.");
                }
                const remainingLabels = issue.labels.filter(
                  label => {
                    return label !== labelToRemove;
                  }
                );

                req.updateLabels(issueId, remainingLabels);
                target.closest("a").remove();
              } catch (error) {
                alert(error);
              }
            };

            const buttons = (<HTMLElement>node).querySelectorAll(
              ".gl-remove-label"
            );
            for (let button of buttons) {
              button.addEventListener("click", removeLabel);
            }
          }
        }
      }
    }
  };

  // Create a new observer
  const observer = new MutationObserver(callback);

  // Start observing
  observer.observe(board, {
    childList: true,
    subtree: true
  });
}
