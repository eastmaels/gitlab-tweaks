import { Requests } from "./requests";
console.log("asdfasdf");
let token: string, options: any;
chrome.storage.sync.get(["token", "options"], result => {
  token = result.token || "";
  options = result.options;
});

const [projectPath, epicId] = document.location.pathname
  .substring(1)
  .split("/-/epics/");
const group = projectPath.split("/")[1];
console.log(group);
const req = new Requests(token, group);

const board = document.querySelector(".epic-tabs-content");

let iconUrl = document
  .querySelector(".dashboard-shortcuts-issues svg use")
  .getAttribute("xlink:href")
  .split("#")[0];

function getIssuesCallback(result) {
  const [issues, labels] = result;
  
  if(options.showWeightSum) {
    // add sum of weight to issue header
    const weight = {
      total: 0,
      open: 0,
      closed: 0
    }

    issues.map((issue) => {
      weight.total += issue.weight;
      issue.state === 'closed' ? weight.closed += issue.weight : weight.open += issue.weight;
    })
    
    const issuesHeader = document.querySelector('.card-header .issue-count-badge')

    issuesHeader.insertAdjacentHTML('afterend', 
    `<div title="Weight (open / closed / total)" style="white-space: break-spaces;font-weight: bold;" class="weight-sum-badge gl-display-inline-flex text-secondary p-0 pr-3"><span class="d-inline-flex align-items-center">
    <svg aria-hidden="true" class="text-secondary mr-1 s16 ic-issues"><use xlink:href="${iconUrl}#weight">
    </use></svg><span class="gl-text-green-500">${weight.open}</span> / <span class="gl-text-blue-500">${weight.closed}</span> / ${weight.total} </span></div>`)
  }

  if (options.showEpicLabels) {
    for (const el of issues) {
      const issue = document.querySelector(`.related-items-tree .gl-link[href="${el.web_url.split('gitlab.com')[1]}"]`)
      console.log(issue)
      const contents = issue.closest('.item-contents');

      const labelMarkup = el.labels.map(label => {
        const obj = labels.find(el => el.name === label) || {name: label, text_color: 'white', color: 'lightgray'}
        const splitted = label.split('::');
        if (splitted.length > 1) {
          // scoped label
          return `<span class="gl-label gl-label-scoped gl-label-sm" style="--label-inset-border: inset 0 0 0 1px ${obj.color};color: ${obj.color};">
          <span class="gl-label-text gl-label-text-light" data-container="body" data-html="true" style="background-color: ${obj.color}">${splitted[0]}</span>
          <span class="gl-label-text " data-container="body" data-html="true">${splitted[1]}</span>
          </span>`;
        }
        return `<span class="gl-label gl-label-sm" style="color:${obj.text_color};background-color:${obj.color};"><span class="gl-label-text gl-label-text-light">${obj.name}</span></span>`
      }).join(' ')
      contents.querySelector('.item-meta').insertAdjacentHTML('beforeend', `<div style="order:1;width:100%;">${labelMarkup}</div>`)
    }
  }
}

if (board) {
  // When mutation is observed
  const callback = function (mutationsList: MutationRecord[]) {
    for (var mutation of mutationsList) {
      if (mutation.addedNodes.length) {
        for (let node of mutation.addedNodes) {
          if (
            (<Element>node).classList?.contains("card-header") &&
            !(<Element>node).classList?.contains("gl-tweaked") && (options.showEpicLabels || options.showWeightSum)
          ) {
            Promise.all([req.getEpicIssues(epicId), req.getGroupLabels(group)]).then(getIssuesCallback)

          }
        }
      }
    }
  };

  // Create a new observer
  const observer = new MutationObserver(callback);

  // Start observing
  observer.observe(board, {
    childList: true,
    subtree: true
  });
}
